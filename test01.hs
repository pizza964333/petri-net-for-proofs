
type ADSJ = [ArchTPId]
type MDSJ = [ArchPTId]
type MCNJ = [ArchPTid]
type ACNJ = [ArchTPid]

data PNFP = PNFP
  { places  :: [((PlaceId,  PlaceState ),(ADSJ,    MDSJ   ))]
  , tranzs  :: [((TranzId,  TranzState ),(MCNJ,    ACNJ   ))]
  , archsPT :: [((ArchPTid, ArchPTstate),(PlaceId, TranzId))]
  , archsTP :: [((ArchTPid, ArchTPstate),(TranzId, PlaceId))]
  }

data PlaceState  = PLST { busy :: Bool }
data TranzState  = TZST { usedTimes :: Int }
data ArchPTstate = APTS { usedTimes :: Int }
data ArchTPstate = ATPS { usedTimes :: Int }

{-

rules

MCNJ Token   Token   -> Token
MDSJ NoTranz NoTranz -> NoPlace

ADSJ Token   _       -> Token
ADSJ _       Token   -> Token


ACNJ NoPlace _       -> NoTranz
ACNJ _       NoPlace -> NoTranz




-}





